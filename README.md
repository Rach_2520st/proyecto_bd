# Proyecto final Base de datos

Base de datos creado a partir de un emprendimiento encargado de la venta de cosmeticos.

Tema: Desarrollo de una aplicación de escritorio mediante GTK3 que permita la manipulación de la base de datos creada a partir del problema principal (manejar los datos de un emprendimiento encargado de la venta de cosmeticos).

# Obtención del programa

Clonar el repositorio, cambiar la linea 23 y 24 del archivo database.py al usuario y contraseña personal, en mi caso;
```
23                                              user="coni",
24                                              passwd="coni",
```
Importar la base de datos del archivo db_cosmeticos_completa.sql:
```
$ mysql -u user -p db_cosmeticos < db_cosmeticos_completa.sql
```
Donde user corresponde al usuario de la base de datos.

Luego, ingresar al repositorio proyecto_bd y  ejecutar el comando:
```
$ python3 app.py

```

# Acerca de

El programa nació a partir de la necesidad de obtener una fuente de ingresos debido a la pandemia, al establecer un emprendimiento se dió a conocer la problematica de la organización de las ventas y el inventario; A raíz de esto surgió la idea de la creación de una base de datos.

El modelo conceptual de esta base de datos es:

![Imagen 1](/modelo/MER.png)

En donde cada venta tiene un cliente y un vendedor, sin embargo un vendedor y un cliente pueden tener una o muchas ventas (en caso de cliente, boletas), en la venta se encuentra especificada la empresa de envio (una empresa puede estar en distintas ventas pero una venta, solo puede tener una empresa de envio), cada venta contiene uno o muchos productos y un producto puede estar en muchas ventas; Cada producto tiene una marca, la tabla contenido_venta posee los detalles de los productos vendidos en cada venta.


El programa presenta inicialmente una ventana que le da la opción al usuario de ingresar al programa, otra de las opciones que brinda el programa es la de agregar usuario en donde si se selecciona esta opción se permite agregar un usuario a la base de datos, al seleccionar esta opción se presenta una ventana que pide datos acerca de tipo de usuario, rut, nombre, dirección y telefono, al hacer click en el botón "ingresar" se guardan los datos ingresados y si no se quieren hacer cambios se recomienda presionar el botón "cancelar" que permite que el usuario vuelva a la ventana principal. Al presionar el botón "salir" de la ventana principal se sale del programa.

Al seleccionar "ingreso" se despliega una ventana que permite el ingreso del usuario, la ventana contiene un GtkEntry que permite la entrada del RUT (id) del usuario, el id de cada usuario corresponde a su RUT sin punto y sin código verificador, al seleccionar el botón "Aceptar" el programa verifica si este RUT está contenido en la base de datos y dependiendo si este es cliente o vendedor abre una ventana independiente para cliente y vendedor, al presionar el botón "cancelar" se retorna a la ventana anterior.

Si el programa define que el usuario ingresado corresponde a un vendedor se abre una ventana contenedora de diferentes botones, entre ellos opciones de productos (Abrir detalles, Agregar), ventas (Abrir detalles, Agregar) y Marca (Abrir detalles, Agregar), al seleccionar el botón "Abrir detalles" se muestra el listado de lo que se quiere ver, ya sea productos, ventas y marcas, las opciones en esta ventana son "modificar seleccionado" que permite la modificación del producto seleccionado, "eliminar seleccionado" este botón permite eliminar el producto que se está seleccionando y "cancelar" que permite volver a la ventana anterior. Al seleccionar el botón "Agregar" aparece una ventana con diferentes GTKEntry dependiendo de que se quiera ingresar (producto, ventas y marcas). los datos pedidos se ingresan al presionar "Agregar", esto quiere decir que el programa los guarda en la base de datos; Si se selecciona "Cancelar" se retorna a la ventana anterior.

Si el programa define que el usuario ingresado corresponde a cliente se muestra una ventana que muestra el historial de compra del usuario en donde se muestran todas las boletas (tabla venta) efectuadas por el cliente y además dos botones, el primero es "Detalle boleta" que permite ver los detalles de su boleta al usuario y el botón "Cancelar" que al presionarlo se vuelve a la ventana de inicio.

# Requisitos
- Linux
- Python 3
- MySQL
- Gtk 3.0
- Matplotlib

# Construccion

Construido y probado con:
- Ubuntu 18.04.03 LTS
- Python 3.6.9
- MySQL 5.7
- Editor utilizado: Sublime Text

# Autores
- Rachell Aravena Martinez
- Constanza Valenzuela Araya
