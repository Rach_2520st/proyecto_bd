#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database
from vendedor import vendedor
from cliente import cliente
# from utils import Mensage


class Main:
    def __init__(self):
        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win_main = builder.get_object("win_main")
        self.win_main.connect("destroy", Gtk.main_quit)

        self.btn_ingreso = builder.get_object("ingres")
        self.btn_ingreso.connect("clicked", self.inicio_sesion)

        self.btn_add_user = builder.get_object("agregar_usuario")
        self.btn_add_user.connect("clicked", self.add_user)

        self.btn_salir = builder.get_object("irse")
        self.btn_salir.connect("clicked", Gtk.main_quit)

        # self.set_columnas_usuarios()
        # self.get_usuarios()

        self.win_main.show_all()

        try:
            Gtk.main()
        except KeyboardInterrupt:
            print("saliendo por ctrl+c")
            sys.exit(0)

    def add_user(self, button):
        form_user = add_user(True, self)

    def inicio_sesion(self, button):
        auxiliar = inicio_sesion()


class add_user:
    """docstring for add_user"""
    def __init__(self, nuevo, obj_padre):
        self.rut = None

        # self.obj_padre = obj_padre
        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win_add_usuario = builder.get_object("add_usuario")
        self.win_add_usuario.show_all()

        self.cmbbox_usuario = builder.get_object("tipo_usuario")

        self.entry_rut = builder.get_object("ingresar_rut")
        self.entry_nombre = builder.get_object("ingresar_nombre")
        self.entry_direccion = builder.get_object("ingresar_direccion")
        self.entry_telefono = builder.get_object("ingresar_telefono")

        self.btn_users_cancel = builder.get_object("sali")
        self.btn_users_cancel.connect("clicked", self.on_btn_user_cancelar)

        self.boton_aceptar = builder.get_object("ingresar")
        self.boton_aceptar.connect("clicked", self.on_btn_user_apply_new_clicked)

    def on_btn_user_apply_new_clicked(self, button):
        rut = self.entry_rut.get_text()
        nombre = self.entry_nombre.get_text()
        direccion = self.entry_direccion.get_text()
        telefono = self.entry_telefono.get_text()

        if len(rut) < 1:
            alerta('Error', 'debe escribir un RUT')
            return

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        if len(direccion) < 1:
            alerta('Error', 'debe escribir una direccion')
            return

        if len(telefono) < 1:
            alerta('Error', 'debe escribir un telefono')
            return

        vendedor = self.cmbbox_usuario.get_active_text()

        if vendedor is None:
            alerta('Error', 'selecciona un tipo de usuario')
            return

        if (vendedor == 'Vendedor'):
            vendedor = 0
        elif (vendedor == 'Cliente'):
            vendedor = 1

        db = Database()

        sql = "select nombre from usuario where rut = %s"
        egg = db.run_select_filter(sql, (rut, ))
        if len(egg) != 0:
            alerta('Error', 'ese rut ya está siendo usado')  # :^)
            return

        db = Database()

        sql = """
              insert into usuario (rut, vendedor, nombre, direccion, telefono) values (%(rut)s, %(vendedor)s, %(nombre)s, %(direccion)s, %(telefono)s)
              """
        db.run_sql(sql, {"rut": rut, "vendedor": vendedor, "nombre": nombre, "direccion": direccion, "telefono": telefono})

        # self.obj_padre.get_jugadores()
        self.win_add_usuario.hide()

    def on_btn_user_cancelar(self, button):
        self.win_add_usuario.hide()


class inicio_sesion:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.inicio_sesion = constructor.get_object("win_inicio_sesion")

        self.entry_rut = constructor.get_object("ini_ses_ingreso_usuario")

        self.btn_aceptar = constructor.get_object("ini_ses_boton_aceptar")
        self.btn_aceptar.connect("clicked", self.get_usuarios)

        self.btn_cerrar = constructor.get_object("ini_ses_boton_cancelar")
        self.btn_cerrar.connect("clicked", self.destruir_ventana)

        # self.get_usuarios()
        self.inicio_sesion.show_all()

    def destruir_ventana(self, *args):
        self.inicio_sesion.destroy()

    def get_usuarios(self, buttom):
        rut = self.entry_rut.get_text()

        db = Database()

        sql = """
              select vendedor, rut from usuario where rut = %(rut)s;
              """
        # result = db.run_select(sql)
        # print(result)

        result = db.run_select_filter(sql, {'rut': rut})

        # self.store.clear()

        if result:
            for r in result:
                # self.store.append(r[1])
                if (r[0] == 0):  # es vendedor
                    # print(result)
                    print(r[0])
                    self.destruir_ventana()
                    trabajador = vendedor()
                elif (r[0] == 1):  # no vendedor
                    print(r[0])
                    # print() se supone q aqui es cuando ingresa e cliente
                    # entonces pense en guardar el valor del entry? y pasarlo a la clase cliente en cliente.py?
                    self.destruir_ventana()
                    comprador = cliente(rut)
        else:
            # error: rut no exite en bdd (o no es rut)
            alerta('error', 'RUT invalido')
            return


def alerta(mensaje, secundario):
    # podría ser .ERROR
    tipo = Gtk.MessageType.INFO
    msgdialog = Gtk.MessageDialog(parent=None, type=tipo, buttons=Gtk.ButtonsType.OK)
    msgdialog.set_property('text', mensaje)

    if secundario:
        msgdialog.set_property('secondary_text', secundario)

    def cerrar(msgdialog, response):
        msgdialog.destroy()

    # eliminar cuando se acepta/cancela/cierra
    msgdialog.connect('response', cerrar)

    msgdialog.show()

    return msgdialog

if __name__ == '__main__':
    Main()
    sys.exit(0)
