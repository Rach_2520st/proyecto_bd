#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk

from database import Database
from utils import Mensage


class cliente:
    """docstring for cliente"""
    def __init__(self, rut):
        self.rut = rut
        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("ventana_cliente")
        self.win.show_all()

        # treeview
        # datos: id, nombre
        self.treeview = cons.get_object("list_ventas_cliente_treeview")
        self.store = Gtk.ListStore(int, str, str, int, int, str, str, str, int)
        self.treeview.set_model(self.store)

        self.ver_prod = cons.get_object("ventana_cliente_boton_detalle")
        self.ver_prod.connect("clicked", self.detalle_venta)

        self.btn_cancelar = cons.get_object("ventana_cliente_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_ventas()

    # def ver_listado_prod_venta():

    def detalle_venta(self, button):
        datos = self.get_row_treeview()
        VentanaDetalleVenta(datos)

    def set_columnas(self):
        for col in self.treeview.get_columns():
            self.treeview.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha", renderer, text=1)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Dirección de retiro", renderer, text=2)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("RUT cliente", renderer, text=3)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("RUT vendedor", renderer, text=4)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Empresa de envío", renderer, text=5)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre retiro", renderer, text=6)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Telefono de contacto", renderer, text=7)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Total", renderer, text=8)
        self.treeview.append_column(column)

    def get_ventas(self):
        rut = self.rut
        db = Database()

        sql = """
              select a.id, a.fecha, a.direccion_envio, a.cliente_rut,
              a.vendedor_rut, b.nombre, a.nombre_retiro, a.telefono, a.total
              from venta a inner join empresa_envio b on a.empresa_envio_id =
              b.id where cliente_rut = %(rut)s;
              """

        result = db.run_select_filter(sql, {"rut": rut})
        print("resultado de ventas", result)

        self.store.clear()

        if result:
            for r in result:
                fecha = r[1].strftime("%F")
                self.store.append([r[0], fecha, r[2], r[3], r[4], r[5], r[6], r[7], r[8]])

    def get_row_treeview(self):
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()

        # si no hay nada seleccionado, devolver nada
        if treeiter is None:
            return None

        # o si no, devolver la fila
        else:
            return model[treeiter]

    def cerrar_ventana(self, button=None):
        self.win.destroy()


class VentanaDetalleVenta:
    def __init__(self, datos_venta):
        self.datos_venta = datos_venta

        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_detalle")
        self.win.show_all()

        # treeview
        self.treeview = cons.get_object("list_detal_treeview")
        self.store = Gtk.ListStore(int, str, int, int)
        self.treeview.set_model(self.store)

        self.btn_cancelar = cons.get_object("list_detal_boton_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_contenido_venta()

    def set_columnas(self):
        tv = self.treeview

        for col in tv.get_columns():
            tv.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Cantidad", renderer, text=2)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Precio", renderer, text=3)
        tv.append_column(column)

    def get_contenido_venta(self):
        id = self.datos_venta[0]
        store = self.store

        sql = """
              select id, nombre, cantidad, contenido_venta.precio from contenido_venta
              join producto on producto_id = id WHERE venta_id = %(id)s
              """

        db = Database()
        result = db.run_select_filter(sql, {"id": id})
        store.clear()

        for r in result:
            store.append(r)

    def cerrar_ventana(self, button=None):
        self.win.destroy()
