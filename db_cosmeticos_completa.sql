-- MySQL dump 10.13  Distrib 5.7.32, for Linux (x86_64)
--
-- Host: localhost    Database: db_cosmeticos
-- ------------------------------------------------------
-- Server version	5.7.32-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contenido_venta`
--

DROP TABLE IF EXISTS `contenido_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contenido_venta` (
  `venta_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  PRIMARY KEY (`venta_id`,`producto_id`),
  KEY `fk_venta_has_producto_producto1_idx` (`producto_id`),
  KEY `fk_venta_has_producto_venta1_idx` (`venta_id`),
  CONSTRAINT `fk_venta_has_producto_producto1` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_has_producto_venta1` FOREIGN KEY (`venta_id`) REFERENCES `venta` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contenido_venta`
--

LOCK TABLES `contenido_venta` WRITE;
/*!40000 ALTER TABLE `contenido_venta` DISABLE KEYS */;
INSERT INTO `contenido_venta` VALUES (1,1,1,1500),(1,2,1,2000),(2,2,1,2000),(2,3,1,1000),(2,4,1,5000),(3,4,1,5000),(3,5,1,2300),(4,7,1,1000),(4,8,1,5000),(4,9,1,5000),(5,4,1,5000),(5,5,1,2300),(5,7,1,1000);
/*!40000 ALTER TABLE `contenido_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa_envio`
--

DROP TABLE IF EXISTS `empresa_envio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa_envio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa_envio`
--

LOCK TABLES `empresa_envio` WRITE;
/*!40000 ALTER TABLE `empresa_envio` DISABLE KEYS */;
INSERT INTO `empresa_envio` VALUES (1,'Starken'),(2,'Chilexpress'),(3,'Atenas'),(4,'Correos de Chile'),(5,'BlueExpress'),(6,'Shipit');
/*!40000 ALTER TABLE `empresa_envio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marca`
--

DROP TABLE IF EXISTS `marca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marca` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marca`
--

LOCK TABLES `marca` WRITE;
/*!40000 ALTER TABLE `marca` DISABLE KEYS */;
INSERT INTO `marca` VALUES (1,'Romantic Beauty'),(2,'Oriflame'),(3,'Flower Secret'),(4,'Wocali'),(5,'Libel');
/*!40000 ALTER TABLE `marca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `stock` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `marca_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_marca_idx` (`marca_id`),
  CONSTRAINT `fk_producto_marca` FOREIGN KEY (`marca_id`) REFERENCES `marca` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'labial As Free Love',24,1500,1),(2,'Máscara de pestañas multifuncional',10,2000,1),(3,'Sombra Ultra Shine',24,1000,1),(4,'tónico facial coco y aloe',3,5000,2),(5,'Delineador waterproof',8,2300,1),(6,'encrespador de pestañas',12,2000,5),(7,'mascarilla para cabello seco',3,1000,2),(8,'Base de maquillaje',5,5000,2),(9,'crema aclaradora',10,5000,2),(10,'contorno de ojos',12,4500,3);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `rut` int(11) NOT NULL,
  `vendedor` tinyint(1) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (2369584,0,'Tatiana Flores','935124679','Curicó, 5 poniente 2 sur #123'),(8741963,1,'José Luis Rojas','956127906','San Rafael, 8 oriente 2 sur #7896'),(12396459,1,'María perez','989564625','Talca, 8 poniente 2 sur'),(12503694,1,'Patricia Argandoña','9865245','Talca, 11 oriente 3 sur #7890'),(12896345,0,'Constanza Valenzuela','96548689','Talca, 12 oriente 1 sur #8963'),(14236987,0,'Matías Báez','955631245','Talca, 11 oriente 10 norte #145'),(16984560,1,'Katerine Rodriguez','986452369','Maule, 5 oriente 2 sur #456'),(20495597,0,'Rachell Aravena','957329355','Curicó, 5 poniente 2 sur #123');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `direccion_envio` varchar(100) NOT NULL,
  `cliente_rut` int(11) NOT NULL,
  `vendedor_rut` int(11) NOT NULL,
  `empresa_envio_id` int(11) NOT NULL,
  `nombre_retiro` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_venta_usuario1_idx` (`cliente_rut`),
  KEY `fk_venta_usuario2_idx` (`vendedor_rut`),
  KEY `fk_venta_empresa_envio1_idx` (`empresa_envio_id`),
  CONSTRAINT `fk_venta_empresa_envio1` FOREIGN KEY (`empresa_envio_id`) REFERENCES `empresa_envio` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_usuario1` FOREIGN KEY (`cliente_rut`) REFERENCES `usuario` (`rut`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_usuario2` FOREIGN KEY (`vendedor_rut`) REFERENCES `usuario` (`rut`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (1,'2020-12-22','11 oriente 3 sur 1895',12503694,20495597,1,'Cristian Sanchez','986531249',3500),(2,'2020-12-26','San Rafael, 8 oriente 2 sur #7896',8741963,12896345,2,'José Luis Rojas','956127906',8000),(3,'2020-12-26','Talca, 8 poniente 2 sur',12396459,20495597,3,'María Perez','989564625',7300),(4,'2020-12-27','Talca, 11 oriente 10 norte #145',14236987,20495597,4,'Matías Baéz','955631245',11000),(5,'2020-12-27','Maule, 5 oriente 2 sur #456',16984560,12896345,1,'Katerin Rodriguez','986452369',8300);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-27 20:26:33
