#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Mensage(Gtk.Window):
    def __init__(self, tipo, msg):
        Gtk.Window.__init__(self, title="Información")

        dialog = Gtk.MessageDialog(parent=self, flags=0, message_type=tipo, buttons=Gtk.ButtonsType.OK, text=msg)
        ###dialog.format_secondary_text("mas información")
        dialog.run()
        dialog.destroy()
