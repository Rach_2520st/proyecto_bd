#!/usr/bin/env python
# -*- coding: utf-8 -*-

#import sys
import gi
import mysql.connector.errors
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import matplotlib.pyplot as plt

from database import Database
#from utils import Mensage


class vendedor:
    def __init__(self):
        constructor = Gtk.Builder()
        constructor.add_from_file("ventanas.glade")

        self.ventana_vendedor = constructor.get_object("ventana_vendedor")
        self.ventana_vendedor.show_all()

        # botones de productos
        # agregar
        self.agregar_producto = constructor.get_object("agregar_producto")
        self.agregar_producto.connect("clicked", self.crear_producto)
        # ver/modificar/eliminar
        self.listado_productos = constructor.get_object("listado_productos")
        self.listado_productos.connect("clicked", self.ver_listado_productos)
        # grafico
        self.graf_productos = constructor.get_object("grafico_productos")
        self.graf_productos.connect("clicked", self.ver_grafico)

        # botones de ventas
        # agregar
        self.agregar_venta = constructor.get_object("agregar_venta")
        self.agregar_venta.connect("clicked", self.crear_venta)
        # ver/modificar/eliminar
        self.listado_ventas = constructor.get_object("listado_ventas")
        self.listado_ventas.connect("clicked", self.ver_listado_ventas)

        # botones de marcas
        # agregar
        self.agregar_marca = constructor.get_object("agregar_marca")
        self.agregar_marca.connect("clicked", self.crear_marca)
        # ver/modificar/eliminar
        self.listado_marcas = constructor.get_object("listado_marcas")
        self.listado_marcas.connect("clicked", self.ver_listado_marcas)

        # botones de empresa
        # agregar
        self.agregar_empresa = constructor.get_object("agregar_empresa")
        self.agregar_empresa.connect("clicked", self.crear_empresa)
        # ver/modificar/eliminar
        self.listado_empresas = constructor.get_object("listado_empresas")
        self.listado_empresas.connect("clicked", self.ver_listado_empresas)

    def crear_producto(self, button):
        VentanaDatosProducto(True)

    def ver_listado_productos(self, button):
        VentanaListadoProductos()

    def ver_grafico(self, btn=None):
        db = Database()

        names = []
        values = []

        sql = """
              select nombre, sum(cantidad) as total from contenido_venta inner join producto on producto_id = producto.id group by nombre
              """
        resultado = db.run_select(sql)

        # tuplas
        for res in resultado:
            values.append(res[1])
            names.append(res[0])

        # quitar barra de herramientas
        plt.rcParams['toolbar'] = 'None'

        # ejes y titulo
        plt.bar(names, values)
        # plt.figure(figsize=(8, 8)) # tamaño ventana
        plt.ylabel("Cantidad vendida")
        plt.xlabel("Nombre producto")
        plt.xticks(rotation='15')
        plt.title("Gráfico Prductos vendidos")
        plt.ion()
        plt.show()

    def crear_venta(self, button):
        VentanaDatosVenta(True)

    def ver_listado_ventas(self, button):
        VentanaListadoVentas()

    def crear_marca(self, button):
        VentanaDatosMarca(True)

    def ver_listado_marcas(self, button):
        VentanaListadoMarcas()

    def crear_empresa(self, button):
        VentanaDatosEmpresa(True)

    def ver_listado_empresas(self, button):
        VentanaListadoEmpresas()


# INICIO PRODUCTO
class VentanaDatosProducto:
    def __init__(self, modo_crear, datos_producto=None, actualizar_ventana_afuera=None):
        # modo_crear decide si se esta modificando o creando un producto
        # datos: id, nombre, stock, precio, nombre_marca
        self.datos_producto = datos_producto

        # esto es una solucion super fea para poder actualizar el listado de productos
        self.actualizar_ventana_afuera = actualizar_ventana_afuera

        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win = builder.get_object("win_agregar_producto")
        self.win.show_all()

        self.entry_nombre = builder.get_object("agrega_prod_nombre")
        self.entry_stock = builder.get_object("agrega_prod_stock")
        self.entry_precio = builder.get_object("agrega_prod_precio")

        # si se está modificando, colocar los datos dados
        if not modo_crear:
            self.entry_nombre.set_text(datos_producto[1])
            self.entry_stock.set_text(str(datos_producto[2]))
            self.entry_precio.set_text(str(datos_producto[3]))  # ??

        self.btn_cancelar = builder.get_object("agrega_prod_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.btn_aceptar = builder.get_object("agrega_prod_agregar")
        if modo_crear:
            self.btn_aceptar.connect("clicked", self.agregar_producto)
        else:
            self.btn_aceptar.connect("clicked", self.modificar_producto)

        self.combobox = builder.get_object("agrega_prod_combobox_marca")
        renderer = Gtk.CellRendererText()
        self.combobox.pack_start(renderer, False)
        self.combobox.add_attribute(renderer, "text", 0)
        renderer = Gtk.CellRendererText()
        self.combobox.pack_start(renderer, False)
        self.combobox.add_attribute(renderer, "text", 1)

        self.store = Gtk.ListStore(int, str)
        self.combobox.set_model(self.store)
        self.get_marcas()

    def get_marcas(self):
        db = Database()

        sql = """
              select id, nombre from marca
              """
        result = db.run_select(sql)

        self.store.clear()

        if result:
            self.store.append([0, "Sin marca"])
            for r in result:
                self.store.append([r[0], r[1]])

    def agregar_producto(self, button):
        nombre = self.entry_nombre.get_text()
        stock = self.entry_stock.get_text()
        precio = self.entry_precio.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        if len(stock) < 1:
            alerta('Error', 'debe escribir un stock')
            return

        if len(precio) < 1:
            alerta('Error', 'debe escribir un precio')
            return

        iterator = self.combobox.get_active_iter()
        if iterator is None:
            alerta('Error', 'Selecciona una empresa de envio')
            return
        else:
            marca_id = self.store[iterator][0]

            if marca_id == 0:
                alerta('Error', 'Debe seleccionar una marca')
                return

        db = Database()

        sql = """
              insert into producto (nombre, stock, precio, marca_id) values
              (%(nombre)s, %(stock)s, %(precio)s, %(marca_id)s)
              """
        db.run_sql(sql, {"nombre": nombre, "stock": stock, "precio": precio,
                         "marca_id": marca_id})

        self.cerrar_ventana()

    def modificar_producto(self, button):
        id = self.datos_producto[0]
        nombre = self.entry_nombre.get_text()
        stock = self.entry_stock.get_text()
        precio = self.entry_precio.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        if len(stock) < 1:
            alerta('Error', 'debe escribir un stock')
            return

        if len(precio) < 1:
            alerta('Error', 'debe escribir un precio')
            return

        iterator = self.combobox.get_active_iter()
        if iterator is None:
            alerta('Error', 'Selecciona una marca')
            return
        else:
            marca_id = self.store[iterator][0]

            if marca_id == 0:
                alerta('Error', 'Debe seleccionar una marca')
                return

        db = Database()

        sql = """
              update producto set nombre = %(nombre)s, stock = %(stock)s,
              precio = %(precio)s, marca_id = %(marca_id)s where id = %(id)s
              """
        db.run_sql(sql, {"nombre": nombre, "stock": stock,
                         "precio": precio, "marca_id": marca_id, "id": id})

        # horrible
        self.actualizar_ventana_afuera()
        self.cerrar_ventana()

    def cerrar_ventana(self, button=None):
        self.win.destroy()


class VentanaListadoProductos:
    def __init__(self):
        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_productos")
        self.win.show_all()

        # treeview
        # datos: id, nombre, stock, precio, nombre_marca
        self.treeview = cons.get_object("list_prod_treeview")
        self.store = Gtk.ListStore(int, str, int, int, str)
        self.treeview.set_model(self.store)

        self.btn_modificar = cons.get_object("list_prod_boton_modificar")
        self.btn_eliminar = cons.get_object("list_prod_boton_eliminar")
        self.btn_cancelar = cons.get_object("list_prod_boton_cancelar")

        self.btn_modificar.connect("clicked", self.modificar_producto)
        self.btn_eliminar.connect("clicked", self.eliminar_producto)
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_productos()

    def set_columnas(self):
        for col in self.treeview.get_columns():
            self.treeview.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Stock", renderer, text=2)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Precio", renderer, text=3)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Marca", renderer, text=4)
        self.treeview.append_column(column)

    def get_productos(self):
        db = Database()

        sql = """
              select a.id, a.nombre, a.stock, a.precio, b.nombre from producto
              a inner join marca b on b.id = a.marca_id;
              """

        result = db.run_select(sql)
        print("resultado de productos", result)

        self.store.clear()

        if result:
            for r in result:
                self.store.append([r[0], r[1], r[2], r[3], r[4]])

    def get_row_treeview(self):
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()

        # si no hay nada seleccionado, devolver nada
        if treeiter is None:
            return None

        # o si no, devolver la fila
        else:
            return model[treeiter]

    def modificar_producto(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        # abrir detalles en modo "modificar"
        VentanaDatosProducto(False, row, self.get_productos)

        # self.get_productos()

    def eliminar_producto(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        id = row[0]
        nombre = row[1]

        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.YES_NO,
                                   text="¿Eliminar este producto? " + str(id) + " " + nombre)
        dialog.format_secondary_text("Esta acción no se puede revertir.")

        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.YES:
            db = Database()
            sql = "delete from producto where id = %(id)s"

            try:
                db.run_sql_DELETE(sql, {'id': id})
            except mysql.connector.errors.IntegrityError:
                alerta('Error', 'no se puede eliminar ese producto, porque está siendo usado en otra parte')
                return
            except mysql.connector.Error as esdkfl:
                alerta('Error', str(esdkfl))
                return

        # después actualizar ventana
        self.get_productos()

    def cerrar_ventana(self, button):
        self.win.destroy()
# FIN PRODUCTO


# INICIO MARCA
class VentanaDatosMarca:
    def __init__(self, modo_crear, datos_marca=None, actualizar_ventana_afuera=None):
        # modo_crear decide si se esta modificando o creando una marca
        # datos: id, nombre
        self.datos_marca = datos_marca

        # esto es una solucion super fea para poder actualizar el listado de marcas
        self.actualizar_ventana_afuera = actualizar_ventana_afuera

        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win = builder.get_object("win_agregar_marca")
        self.win.show_all()

        self.entry_nombre = builder.get_object("agrega_marc_nombre")

        # si se está modificando, colocar los datos dados
        if not modo_crear:
            self.entry_nombre.set_text(datos_marca[1])

        self.btn_cancelar = builder.get_object("agrega_marc_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.btn_aceptar = builder.get_object("agrega_marc_agregar")
        if modo_crear:
            self.btn_aceptar.connect("clicked", self.agregar_marca)
        else:
            self.btn_aceptar.connect("clicked", self.modificar_marca)

    def agregar_marca(self, button):
        nombre = self.entry_nombre.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        db = Database()

        sql = """
              insert into marca (nombre) values (%(nombre)s)
              """
        db.run_sql(sql, {"nombre": nombre})

        self.cerrar_ventana()

    def modificar_marca(self, button):
        id = self.datos_marca[0]
        nombre = self.entry_nombre.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        db = Database()

        sql = """
              update marca set nombre = %(nombre)s where id = %(id)s
              """
        db.run_sql(sql, {"nombre": nombre, "id": id})

        # horrible
        self.actualizar_ventana_afuera()
        self.cerrar_ventana()

    def cerrar_ventana(self, button=None):
        self.win.destroy()


class VentanaListadoMarcas:
    def __init__(self):
        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_marcas")
        self.win.show_all()

        # treeview
        # datos: id, nombre, stock, precio, nombre_marca
        self.treeview = cons.get_object("list_marc_treeview")
        self.store = Gtk.ListStore(int, str)
        self.treeview.set_model(self.store)

        self.btn_modificar = cons.get_object("list_marc_boton_modificar")
        self.btn_eliminar = cons.get_object("list_marc_boton_eliminar")
        self.btn_cancelar = cons.get_object("list_marc_boton_cancelar")

        self.btn_modificar.connect("clicked", self.modificar_marca)
        self.btn_eliminar.connect("clicked", self.eliminar_marca)
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_marcas()

    def set_columnas(self):
        for col in self.treeview.get_columns():
            self.treeview.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview.append_column(column)

    def get_marcas(self):
        db = Database()

        sql = """
              select id, nombre from marca
              """

        result = db.run_select(sql)
        print("resultado de marcas", result)

        self.store.clear()

        if result:
            for r in result:
                self.store.append([r[0], r[1]])

    def get_row_treeview(self):
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()

        # si no hay nada seleccionado, devolver nada
        if treeiter is None:
            return None

        # o si no, devolver la fila
        else:
            return model[treeiter]

    def modificar_marca(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        # abrir detalles en modo "modificar"
        VentanaDatosMarca(False, row, self.get_marcas)

        # self.get_productos()

    def eliminar_marca(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        id = row[0]
        nombre = row[1]

        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.YES_NO,
                                   text="¿Eliminar esta marca? " + str(id) + " " + nombre)
        dialog.format_secondary_text("Esta acción no se puede revertir.")

        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.YES:
            db = Database()
            sql = """
                  delete from marca where id = %(id)s
                  """

            try:
                db.run_sql_DELETE(sql, {'id': id})
            except mysql.connector.errors.IntegrityError:
                alerta('error', 'esa marca no se puede eliminar porque está siendo usado en otra parte')
                return
            except mysql.connector.Error as esdkfl:
                alerta('error', str(esdkfl))
                return

            # después actualizar ventana
            self.get_marcas()

    def cerrar_ventana(self, button):
        self.win.destroy()
# FIN MARCA


# INICIO EMPRESA
class VentanaDatosEmpresa:
    def __init__(self, modo_crear, datos_empresa=None, actualizar_ventana_afuera=None):
        # modo_crear decide si se esta modificando o creando una empresa
        # datos: id, nombre
        self.datos_empresa = datos_empresa

        # esto es una solucion super fea para poder actualizar el listado de empresa
        self.actualizar_ventana_afuera = actualizar_ventana_afuera

        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win = builder.get_object("win_agregar_empresa")
        self.win.show_all()

        self.entry_nombre = builder.get_object("agrega_empr_nombre")

        # si se está modificando, colocar los datos dados
        if not modo_crear:
            self.entry_nombre.set_text(datos_empresa[1])

        self.btn_cancelar = builder.get_object("agrega_empr_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.btn_aceptar = builder.get_object("agrega_empr_agregar")
        if modo_crear:
            self.btn_aceptar.connect("clicked", self.agregar_empresa)
        else:
            self.btn_aceptar.connect("clicked", self.modificar_empresa)

    def agregar_empresa(self, button):
        nombre = self.entry_nombre.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        db = Database()

        sql = """
              insert into empresa_envio (nombre) values (%(nombre)s)
              """
        db.run_sql(sql, {"nombre": nombre})

        self.cerrar_ventana()

    def modificar_empresa(self, button):
        id = self.datos_empresa[0]
        nombre = self.entry_nombre.get_text()

        if len(nombre) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        db = Database()

        sql = """
              update empresa_envio set nombre = %(nombre)s where id = %(id)s
              """
        db.run_sql(sql, {"nombre": nombre, "id": id})

        # horrible
        self.actualizar_ventana_afuera()
        self.cerrar_ventana()

    def cerrar_ventana(self, button=None):
        self.win.destroy()


class VentanaListadoEmpresas:
    def __init__(self):
        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_empresas")
        self.win.show_all()

        # treeview
        # datos: id, nombre
        self.treeview = cons.get_object("list_empr_treeview")
        self.store = Gtk.ListStore(int, str)
        self.treeview.set_model(self.store)

        self.btn_modificar = cons.get_object("list_empr_boton_modificar")
        self.btn_eliminar = cons.get_object("list_empr_boton_eliminar")
        self.btn_cancelar = cons.get_object("list_empr_boton_cancelar")

        self.btn_modificar.connect("clicked", self.modificar_empresa)
        self.btn_eliminar.connect("clicked", self.eliminar_empresa)
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_empresas()

    def set_columnas(self):
        for col in self.treeview.get_columns():
            self.treeview.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        self.treeview.append_column(column)

    def get_empresas(self):
        db = Database()

        sql = """
              select id, nombre from empresa_envio
              """

        result = db.run_select(sql)
        print("resultado de empresas", result)

        self.store.clear()

        if result:
            for r in result:
                self.store.append([r[0], r[1]])

    def get_row_treeview(self):
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()

        # si no hay nada seleccionado, devolver nada
        if treeiter is None:
            return None

        # o si no, devolver la fila
        else:
            return model[treeiter]

    def modificar_empresa(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        # abrir detalles en modo "modificar"
        VentanaDatosEmpresa(False, row, self.get_empresas)

        # self.get_empresa()

    def eliminar_empresa(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        id = row[0]
        nombre = row[1]

        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.YES_NO,
                                   text="¿Eliminar esta empresa? " + str(id) + " " + nombre)
        dialog.format_secondary_text("Esta acción no se puede revertir.")

        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.YES:
            # DELETE FROM EMPRESA WHERE id = row[0]
            db = Database()
            sql = """
                  delete from empresa_envio where id = %(id)s
                  """
            # db.run_sql(sql, {'id': id})

            try:
                db.run_sql_DELETE(sql, {'id': id})
            except mysql.connector.errors.IntegrityError:
                alerta('error', 'esa marca no se puede eliminar porque está siendo usado en otra parte')
                return
            except mysql.connector.Error as esdkfl:
                alerta('error', str(esdkfl))
                return

            # después actualizar ventana
            self.get_empresas()

    def cerrar_ventana(self, button):
        self.win.destroy()
# FIN EMPRESA


# INICIO VENTA
class VentanaDatosVenta:
    def __init__(self, modo_crear, datos_venta=None, actualizar_ventana_afuera=None):
        # modo_crear decide si se esta modificando o creando una venta
        # datos: id, fecha, rut cliente, rut vendedor, direccion, empresa envio, nombre retiro, telefono, total
        self.datos_venta = datos_venta

        # esto es una solucion super fea para poder actualizar el listado de ventas
        self.actualizar_ventana_afuera = actualizar_ventana_afuera

        builder = Gtk.Builder()
        builder.add_from_file("ventanas.glade")

        self.win = builder.get_object("win_agregar_venta")
        self.win.show_all()

        self.entry_direccion = builder.get_object("agrega_vent_direccion")
        self.entry_rut_cliente = builder.get_object("agrega_vent_rut_cliente")
        self.entry_rut_vendedor = builder.get_object("agrega_vent_rut_vendedor")
        self.entry_nombre = builder.get_object("agrega_vent_nombre")
        self.entry_telefono = builder.get_object("agrega_vent_telefono")
        self.entry_total = builder.get_object("agrega_vent_total")

        # si se está modificando, colocar los datos dados
        if not modo_crear:
            self.entry_direccion.set_text(datos_venta[2])
            self.entry_rut_cliente.set_text(str(datos_venta[3]))
            self.entry_rut_vendedor.set_text(str(datos_venta[4]))
            self.entry_nombre.set_text(datos_venta[6])
            self.entry_telefono.set_text(str(datos_venta[7]))
            self.entry_total.set_text(str(datos_venta[8]))

        self.btn_cancelar = builder.get_object("agrega_vent_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.btn_aceptar = builder.get_object("agrega_vent_agregar")
        if modo_crear:
            self.btn_aceptar.connect("clicked", self.agregar_venta)
        else:
            self.btn_aceptar.connect("clicked", self.modificar_venta)

        self.combobox = builder.get_object("agrega_vent_combobox_empresa")
        renderer = Gtk.CellRendererText()
        self.combobox.pack_start(renderer, False)
        self.combobox.add_attribute(renderer, "text", 0)
        renderer = Gtk.CellRendererText()
        self.combobox.pack_start(renderer, False)
        self.combobox.add_attribute(renderer, "text", 1)

        self.store = Gtk.ListStore(int, str)
        self.combobox.set_model(self.store)
        self.get_empresas()

        self.treeview_listado_prod = builder.get_object("agrega_vent_treeview_listado_productos")
        self.store_listado_prod = Gtk.ListStore(int, str, int, int)
        self.treeview_listado_prod.set_model(self.store_listado_prod)

        self.treeview_prod_seleccionados = builder.get_object("agrega_vent_treeview_productos_seleccionados")
        self.store_prod_seleccionados = Gtk.ListStore(int, str, int, int)
        self.treeview_prod_seleccionados.set_model(self.store_prod_seleccionados)

        self.boton_agrega_producto = builder.get_object("agrega_vent_boton_agrega_producto")
        self.boton_elimina_producto = builder.get_object("agrega_vent_boton_elimina_producto")

        self.configurar_treeview_productos()

        if modo_crear is False:
            self.get_contenido_venta()

    def get_contenido_venta(self):
        id = self.datos_venta[0]
        store = self.store_prod_seleccionados

        sql = """
              select id, nombre, cantidad, contenido_venta.precio from contenido_venta
              join producto on producto_id = id WHERE venta_id = %(id)s
              """

        db = Database()
        result = db.run_select_filter(sql, {"id": id})
        store.clear()

        for r in result:
            store.append(r)

    def configurar_treeview_productos(self):
        # primero el treeview de los productos
        tv = self.treeview_listado_prod
        for col in tv.get_columns():
            tv.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Stock", renderer, text=2)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Precio", renderer, text=3)
        tv.append_column(column)

        # rellenar el treeview de listado de productos
        db = Database()

        sql = """
              select id, nombre, stock, precio from producto
              """

        result = db.run_select(sql)
        self.store_listado_prod.clear()

        for r in result:
            self.store_listado_prod.append(r)

        # columnas del treeview de los productos seleccionados en esta venta
        tv = self.treeview_prod_seleccionados

        for col in tv.get_columns():
            tv.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Cantidad", renderer, text=2)
        tv.append_column(column)

        # hacer que se agregue desde uno al otro
        def get_row_treeview(tv):
            selection = tv.get_selection()
            model, treeiter = selection.get_selected()

            # si no hay nada seleccionado, devolver nada
            if treeiter is None:
                return None

            # o si no, devolver la fila
            else:
                return model[treeiter]

        def agregar_producto():
            prod_seleccionado = get_row_treeview(self.treeview_listado_prod)

            if prod_seleccionado is None:
                return

            id = prod_seleccionado[0]
            nombre = prod_seleccionado[1]
            precio = prod_seleccionado[3]

            for prod in self.store_prod_seleccionados:
                # si el producto ya está en el treeview
                if prod[0] == id:
                    # sumarle 1 a la cantidad
                    prod[2] = prod[2] + 1
                    return

            # de lo contrario, agregarlo al listado
            # primero agregar sólo 1, despues se suman más productos
            self.store_prod_seleccionados.append([id, nombre, 1, precio])

        def eliminar_producto():
            selection = self.treeview_prod_seleccionados.get_selection()
            model, treeiter = selection.get_selected()

            if treeiter is None:
                return None

            self.store_prod_seleccionados.remove(treeiter)

        def actualizar_total():
            total = 0

            for prod in self.store_prod_seleccionados:
                total = total + (prod[2] * prod[3])

            self.entry_total.set_text(str(total))

        def agregar_y_actualizar(xd=None):
            agregar_producto()
            actualizar_total()

        def eliminar_y_actualizar(lol=None):
            agregar_producto()
            actualizar_total()

        # cuando se haga click en agregar,
        # mover los datos desde un treeview al otro
        self.boton_agrega_producto.connect("clicked", agregar_y_actualizar)

        # eliminar los datos del treeview al hacer click en eliminar
        self.boton_elimina_producto.connect("clicked", eliminar_y_actualizar)

    def get_empresas(self):
        db = Database()

        sql = """
              select id, nombre from empresa_envio
              """
        result = db.run_select(sql)

        self.store.clear()

        if result:
            self.store.append([0, "Sin empresa determinada"])
            for r in result:
                self.store.append([r[0], r[1]])

    def agregar_venta(self, button):
        direccion_envio = self.entry_direccion.get_text()
        cliente_rut = self.entry_rut_cliente.get_text()
        vendedor_rut = self.entry_rut_vendedor.get_text()
        nombre_retiro = self.entry_nombre.get_text()
        telefono = self.entry_telefono.get_text()
        # total = self.entry_total.get_text()

        if len(direccion_envio) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        if len(cliente_rut) < 1:
            alerta('Error', 'debe escribir un RUT de un cliente')
            return

        if len(vendedor_rut) < 1:
            alerta('Error', 'debe escribir un RUT de un vendedor')
            return

        if len(nombre_retiro) < 1:
            alerta('Error', 'debe escribir el nombre de quien recibe el paquete')
            return

        if len(telefono) < 1:
            alerta('Error', 'debe escribir un telefono')
            return

        iterator = self.combobox.get_active_iter()
        if iterator is None:
            alerta('Error', 'Selecciona una empresa de envio')
            return
        else:
            empresa_envio_id = self.store[iterator][0]

            if empresa_envio_id == 0:
                # el modelo dice que la empresa no puede ser nula
                # venta.empresa_envio_id INT NOT NULL
                alerta('Error', 'Debe seleccionar una empresa de envío')
                return

        productos_seleccionados = self.store_prod_seleccionados

        for prod in productos_seleccionados:
            print("producto deseado en esta venta:", prod[0], prod[1], prod[2], prod[3])

        db = Database()

        sql = """
              select rut from usuario where rut = %(cliente_rut)s and vendedor = 1
              """
        hay_cliente = db.run_select_filter(sql, {"cliente_rut": cliente_rut})

        if len(hay_cliente) != 1:
            alerta('error', 'no existe un cliente con ese rut')
            return

        db = Database()

        sql = """
              select rut from usuario where rut = %(vendedor_rut)s and vendedor = 0
              """
        hay_vendedor = db.run_select_filter(sql, {"vendedor_rut": vendedor_rut})

        if len(hay_vendedor) != 1:
            alerta('error', 'no existe un vendedor con ese rut')
            return

        total = 0

        for prod in productos_seleccionados:
            total = total + (prod[2] * prod[3])

        # # ver total
        # sql = """
        #       select sum(precio * cantidad) from contenido_venta where venta_id = %(venta_id)s
        #       """
        # test = db.run_sql(sql, {"venta_id": venta_id})
        # venta_id = test[0]
        # self.entry_total(sql)

        print("se va a insertar esto:",
              {"direccion_envio": direccion_envio,
               "cliente_rut": cliente_rut,
               "vendedor_rut": vendedor_rut,
               "empresa_envio_id": empresa_envio_id,
               "nombre_retiro": nombre_retiro,
               "telefono": telefono,
               "total": total
               })


        # guardar la venta
        sql = """
              insert into venta (direccion_envio, cliente_rut, vendedor_rut, empresa_envio_id, nombre_retiro, telefono, total) values
              (%(direccion_envio)s, %(cliente_rut)s, %(vendedor_rut)s, %(empresa_envio_id)s, %(nombre_retiro)s, %(telefono)s, %(total)s)
              """
        db.run_sql(sql, {"direccion_envio": direccion_envio,
                         "cliente_rut": cliente_rut,
                         "vendedor_rut": vendedor_rut,
                         "empresa_envio_id": empresa_envio_id,
                         "nombre_retiro": nombre_retiro,
                         "telefono": telefono,
                         "total": total
                         })

        db = Database()
        sql = """
              select id from venta order by id desc limit 1
              """
        test = db.run_select(sql)

        if len(test) != 1:
            alerta('Error', 'no se pudo guardar la venta')
            return

        venta_id = test[0][0]

        # insertar los productos seleccionados en la tabla de contenido_venta
        # sql = insert into contenido_venta blablabla

        for prod in productos_seleccionados:
            print("producto:", {"venta_id": venta_id,
                                "producto_id": prod[0],
                                "cantidad": prod[2],
                                "precio": prod[3]
                                })
            # prod[0] = id
            # prod[1] = nombre
            # prod[2] = cantidad
            # prod[3] = precio
            sql = """
                  insert into contenido_venta (venta_id, producto_id, cantidad, precio) values
                  (%(venta_id)s, %(producto_id)s, %(cantidad)s, %(precio)s)
                  """
            db.run_sql(sql, {"venta_id": venta_id,
                             "producto_id": prod[0],
                             "cantidad": prod[2],
                             "precio": prod[3]
                             })

            self.cerrar_ventana()

    def modificar_venta(self, button):
        direccion_envio = self.entry_direccion.get_text()
        cliente_rut = self.entry_rut_cliente.get_text()
        vendedor_rut = self.entry_rut_vendedor.get_text()
        nombre_retiro = self.entry_nombre.get_text()
        telefono = self.entry_telefono.get_text()
        total = self.entry_total.get_text()

        if len(direccion_envio) < 1:
            alerta('Error', 'debe escribir un nombre')
            return

        if len(cliente_rut) < 1:
            alerta('Error', 'debe escribir un RUT de un cliente')
            return

        if len(vendedor_rut) < 1:
            alerta('Error', 'debe escribir un RUT de un vendedor')
            return

        if len(nombre_retiro) < 1:
            alerta('Error', 'debe escribir el nombre de quien recibe el paquete')
            return

        if len(telefono) < 1:
            alerta('Error', 'debe escribir un telefono')
            return

        print("eeeeeeee", self.datos_venta)
        iterator = self.combobox.get_active_iter()
        if iterator is None:
            alerta('Error', 'Debe seleccionar una empresa de envio')
            return
        else:
            empresa_envio_id = self.store[iterator][0]

            if empresa_envio_id == 0:
                alerta('Error', 'Debe seleccionar una empresa de envio')
                return

        id = self.datos_venta[0]

        print("se va a modificar la venta ", id, " con estos valores:")
        print({"direccion_envio": direccion_envio,
               "cliente_rut": cliente_rut,
               "vendedor_rut": vendedor_rut,
               "empresa_envio_id": empresa_envio_id,
               "nombre_retiro": nombre_retiro,
               "telefono": telefono,
               "total": total,
               "id": id})

        db = Database()

        sql = """
              update venta set direccion_envio = %(direccion_envio)s,
              cliente_rut = %(cliente_rut)s, vendedor_rut = %(vendedor_rut)s,
              empresa_envio_id = %(empresa_envio_id)s, nombre_retiro = %(nombre_retiro)s,
              telefono = %(telefono)s, total = %(total)s where id = %(id)s
              """
        db.run_sql(sql, {"direccion_envio": direccion_envio,
                         "cliente_rut": cliente_rut,
                         "vendedor_rut": vendedor_rut,
                         "empresa_envio_id": empresa_envio_id,
                         "nombre_retiro": nombre_retiro,
                         "telefono": telefono,
                         "total": total,
                         "id": id})

        productos_seleccionados = self.store_prod_seleccionados

        for prod in productos_seleccionados:
            print("producto deseado en esta venta:", prod[0], prod[1], prod[2], prod[3])

        # borrar todos los productos
        db = Database()
        db.run_sql("delete from contenido_venta where venta_id = %(venta_id)s", {"venta_id": id})

        # insertar los productos con los datos nuevos
        for prod in productos_seleccionados:
            # prod[0] = id
            # prod[1] = nombre
            # prod[2] = cantidad
            # prod[3] = precio
            sql = """
                  insert into contenido_venta (venta_id, producto_id, cantidad, precio) values
                  (%(venta_id)s, %(producto_id)s, %(cantidad)s, %(precio)s)
                  """
            db.run_sql(sql, {"venta_id": id,
                             "producto_id": prod[0],
                             "cantidad": prod[2],
                             "precio": prod[3]
                             })
            # hay que revisar si este db.run_sql fue exitoso

        # horrible
        self.actualizar_ventana_afuera()
        self.cerrar_ventana()

    def cerrar_ventana(self, button=None):
        self.win.destroy()


class VentanaListadoVentas:
    def __init__(self):
        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_ventas")
        self.win.show_all()

        # treeview
        self.treeview = cons.get_object("list_vent_treeview")
        self.store = Gtk.ListStore(int, str, str, int, int, str, str, str, int)
        self.treeview.set_model(self.store)

        self.btn_modificar = cons.get_object("list_vent_boton_modificar")
        self.btn_eliminar = cons.get_object("list_vent_boton_eliminar")
        self.btn_detalle = cons.get_object("list_vent_boton_detalle")
        self.btn_cancelar = cons.get_object("list_vent_boton_cancelar")

        self.btn_modificar.connect("clicked", self.modificar_venta)
        self.btn_eliminar.connect("clicked", self.eliminar_venta)
        self.btn_detalle.connect("clicked", self.detalle_venta)
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_ventas()

    def detalle_venta(self, button):
        datos = self.get_row_treeview()
        VentanaDetalleVenta(datos)

    def set_columnas(self):
        for col in self.treeview.get_columns():
            self.treeview.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Fecha", renderer, text=1)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Dirección de retiro", renderer, text=2)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("RUT cliente", renderer, text=3)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("RUT vendedor", renderer, text=4)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Empresa de envío", renderer, text=5)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre retiro", renderer, text=6)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Telefono de contacto", renderer, text=7)
        self.treeview.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Total", renderer, text=8)
        self.treeview.append_column(column)

    def get_ventas(self):
        db = Database()

        sql = """
              select a.id, a.fecha, a.direccion_envio, a.cliente_rut,
              a.vendedor_rut, b.nombre, a.nombre_retiro, a.telefono, a.total
              from venta a inner join empresa_envio b on a.empresa_envio_id =
              b.id;
              """

        result = db.run_select(sql)
        print("resultado de ventas", result)

        self.store.clear()

        if result:
            for r in result:
                fecha = r[1].strftime("%F")
                self.store.append([r[0], fecha, r[2], r[3], r[4], r[5], r[6], r[7], r[8]])

    def get_row_treeview(self):
        selection = self.treeview.get_selection()
        model, treeiter = selection.get_selected()

        # si no hay nada seleccionado, devolver nada
        if treeiter is None:
            return None

        # o si no, devolver la fila
        else:
            return model[treeiter]

    def modificar_venta(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        # abrir detalles en modo "modificar"
        VentanaDatosVenta(False, row, self.get_ventas)
        # def __init__(self, modo_crear, datos_venta=None, actualizar_ventana_afuera=None):

        # self.get_ventas()

    def eliminar_venta(self, button):
        row = self.get_row_treeview()

        if row is None:
            # salir si no hay nada seleccionado
            return

        id = row[0]

        dialog = Gtk.MessageDialog(message_type=Gtk.MessageType.QUESTION,
                                   buttons=Gtk.ButtonsType.YES_NO,
                                   text="¿Eliminar esta venta? " + str(id))
        dialog.format_secondary_text("Esta acción no se puede revertir.")

        response = dialog.run()
        dialog.destroy()

        if response == Gtk.ResponseType.YES:
            # DELETE FROM venta WHERE id = row[0]
            db = Database()
            sql = """
                  delete from venta where id = %(id)s
                  """

            try:
                db.run_sql_DELETE(sql, {'id': id})
            except mysql.connector.errors.IntegrityError:
                alerta('error', 'esa venta no se puede eliminar porque está siendo usada en otra parte')
                return
            except mysql.connector.Error as esdkfl:
                alerta('error', str(esdkfl))
                return

            # después actualizar ventana
            self.get_ventas()

    # def detalle_venta(self, button):

    def cerrar_ventana(self, button):
        self.win.destroy()
# FIN VENTA


class VentanaDetalleVenta:
    def __init__(self, datos_venta):
        self.datos_venta = datos_venta

        cons = Gtk.Builder()
        cons.add_from_file("ventanas.glade")

        # ventana
        self.win = cons.get_object("win_listado_detalle")
        self.win.show_all()

        # treeview
        self.treeview = cons.get_object("list_detal_treeview")
        self.store = Gtk.ListStore(int, str, int, int)
        self.treeview.set_model(self.store)

        self.btn_cancelar = cons.get_object("list_detal_boton_cancelar")
        self.btn_cancelar.connect("clicked", self.cerrar_ventana)

        self.set_columnas()
        self.get_contenido_venta()

    def set_columnas(self):
        tv = self.treeview

        for col in tv.get_columns():
            tv.remove_column(col)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("ID", renderer, text=0)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Nombre", renderer, text=1)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Cantidad", renderer, text=2)
        tv.append_column(column)

        renderer = Gtk.CellRendererText()
        column = Gtk.TreeViewColumn("Precio", renderer, text=3)
        tv.append_column(column)

    def get_contenido_venta(self):
        id = self.datos_venta[0]
        store = self.store

        sql = """
              select id, nombre, cantidad, contenido_venta.precio from contenido_venta
              join producto on producto_id = id WHERE venta_id = %(id)s
              """

        db = Database()
        result = db.run_select_filter(sql, {"id": id})
        store.clear()

        for r in result:
            store.append(r)

    def cerrar_ventana(self, button=None):
        self.win.destroy()


def alerta(mensaje, secundario):
    # podría ser .ERROR
    tipo = Gtk.MessageType.INFO
    msgdialog = Gtk.MessageDialog(parent=None, type=tipo, buttons=Gtk.ButtonsType.OK)
    msgdialog.set_property('text', mensaje)

    if secundario:
        msgdialog.set_property('secondary_text', secundario)

    def cerrar(msgdialog, response):
        msgdialog.destroy()

    # eliminar cuando se acepta/cancela/cierra
    msgdialog.connect('response', cerrar)

    msgdialog.show()

    return msgdialog
